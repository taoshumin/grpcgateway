# [Grpc Gateway](https://github.com/grpc-ecosystem/grpc-gateway)

注释：[go1.18弃用go get](https://go.dev/doc/go-get-install-deprecation)

- 升级go tool

```shell
compile: version "go1.18" does not match go tool version "go1.18.5"
# sync/atomic
compile: version "go1.18" does not match go tool version "go1.18.5"
```

解决方法： `本机中有两个go文件,whereis go`

- 安装

```shell
go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway@latest
go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
```

查看安装`$GOPATH/bin`：

```shell
base) ╭─taoshumin_vendor at taoshumin_vendor的MacBook Pro in ~/go/bin
╰─○ ll protoc-gen-*
-rwxr-xr-x  1 taoshumin_vendor  staff   7.9M Aug 30 10:07 protoc-gen-go
-rwxr-xr-x  1 taoshumin_vendor  staff   7.7M Aug 30 10:07 protoc-gen-go-grpc
-rwxr-xr-x  1 taoshumin_vendor  staff   7.5M Apr 16 17:19 protoc-gen-gofast
-rwxr-xr-x  1 taoshumin_vendor  staff   7.5M Apr 16 17:20 protoc-gen-gogo
-rwxr-xr-x  1 taoshumin_vendor  staff   7.5M Jun  9 10:10 protoc-gen-gogofast
-rwxr-xr-x  1 taoshumin_vendor  staff    10M Aug 30 09:53 protoc-gen-grpc-gateway
-rwxr-xr-x  1 taoshumin_vendor  staff    11M Aug 30 10:07 protoc-gen-openapiv2
```

- OpenAi注释文档示例

- [a_bit_of_everything.proto](https://github.com/grpc-ecosystem/grpc-gateway/blob/master/examples/internal/proto/examplepb/a_bit_of_everything.proto)

- 拷贝 googleapis repository到当前项目

These can be found by manually cloning and copying the relevant files from the googleapis repository,

实际操作：

```shell
git clone git@github.com:googleapis/googleapis.git
cp -r google ../../grpcgateway/proto
```

```shell
google/api/annotations.proto
google/api/field_behavior.proto
google/api/http.proto
google/api/httpbody.proto
```

# 安装protobuf 

```shell
brew install protobuf
╰─○ protoc --version
libprotoc 3.21.5

M1:
arch -arm64 brew reinstall protobuf
```

# 目录介绍

- proto: pb创建文件目录，包含google api
- pb: proto生成的go文件目录

# 学习视频
- [https://www.youtube.com/watch?v=Zf9G2KzYs7w](https://www.youtube.com/watch?v=Zf9G2KzYs7w)


# Goland 自己的proto文件报红

```
路径： /Users/taoshumin_vendor/Library/Caches/JetBrains/GoLand2022.1/protoeditor/google
```

[解决方案连接](https://blog.csdn.net/inthat/article/details/116661214)