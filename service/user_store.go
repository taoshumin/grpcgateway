/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package service

import (
	"fmt"
	"sync"
)

type UserStore interface {
	Save(usr *User) error
	Find(username string) (*User, error)
}

type MemoryUserStore struct {
	lock  sync.RWMutex
	users map[string]*User
}

func (m *MemoryUserStore) Save(usr *User) error {
	m.lock.Lock()
	defer m.lock.Unlock()

	if m.users[usr.Username] != nil {
		return fmt.Errorf("%s has exist", usr.Username)
	}

	m.users[usr.Username] = usr.Clone()
	return nil
}

func (m *MemoryUserStore) Find(username string) (*User, error) {
	m.lock.Lock()
	defer m.lock.Unlock()

	user := m.users[username]
	if user == nil {
		return nil, nil
	}
	return user.Clone(), nil
}

func NewMemoryUserStore() *MemoryUserStore {
	return &MemoryUserStore{
		users: make(map[string]*User),
	}
}
