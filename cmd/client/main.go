/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"book/client"
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// copy from https://github.com/techschool/pcbook-go

func main() {
	if err := NewGRPCClient(); err != nil {
		panic(err)
	}
}

func authMethods() map[string]bool {
	const laptopServicePath = "/techschool.pcbook.LaptopService/"

	return map[string]bool{
		laptopServicePath + "CreateLaptop": true,
		laptopServicePath + "UploadImage":  true,
		laptopServicePath + "RateLaptop":   true,
	}
}

func NewGRPCClient() error {
	transportOption := grpc.WithTransportCredentials(insecure.NewCredentials())
	cc, err := grpc.Dial(":18089", transportOption)
	if err != nil {
		return err
	}
	defer cc.Close()

	authClient := client.NewAuthClient(cc, "admin1", "secret")
	// interceptor, err := client.NewAuthMiddleware(authClient, authMethods(), 30*time.Second)
	res, err := authClient.Login(context.Background())

	fmt.Println("Auth Client Reponse: ", res)

	//cc2, err := grpc.Dial(
	//	":18089",
	//	transportOption,
	//	grpc.WithUnaryInterceptor(interceptor.Unary()),
	//	grpc.WithStreamInterceptor(interceptor.Stream()),
	//)
	//
	//fmt.Println(cc2)
	return err
}

// HTTP 查看OWNE示例
