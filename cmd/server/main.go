/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"book/pb"
	"book/service"
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/reflection"
	"net"
	"net/http"
	"sync"
	"time"
)

const (
	secretKey     = "secret"
	tokenDuration = 15 * time.Minute
)

func main() {
	userStore := service.NewMemoryUserStore()
	_ = NewMockSaveUser(userStore)
	jwtManager := service.NewJwtManager(secretKey, tokenDuration)
	authServer := service.NewAuthService(userStore, jwtManager)

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := NewGRPCServer(authServer); err != nil {
			panic(err)
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := NewRESTServer(authServer); err != nil {
			panic(err)
		}
	}()
	wg.Wait()
}

// NewGRPCServer 创建GRPC服务
func NewGRPCServer(authServer pb.AuthServiceServer) error {
	grpcServer := grpc.NewServer()
	pb.RegisterAuthServiceServer(grpcServer, authServer)
	listener, err := net.Listen("tcp", ":18089")
	if err != nil {
		return err
	}
	reflection.Register(grpcServer)
	return grpcServer.Serve(listener)
}

// NewRESTServer 创建HTTP服务
func NewRESTServer(authServer pb.AuthServiceServer) error {
	mux := runtime.NewServeMux()

	dialOptions := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	err := pb.RegisterAuthServiceHandlerFromEndpoint(ctx, mux, ":18089", dialOptions)
	if err != nil {
		return err
	}

	listener, err := net.Listen("tcp", ":8089")
	if err != nil {
		return err
	}
	return http.Serve(listener, mux)
}

// NewMockUser 创建测试用户
func NewMockUser() (users []*service.User) {
	adm, _ := service.NewUser("admin1", "secret", "admin")
	gest, _ := service.NewUser("guest1", "secret", "guest")
	users = append(users, adm)
	users = append(users, gest)
	return users
}

// NewMockSaveUser 创建测试用户保存在内存中.
func NewMockSaveUser(store service.UserStore) error {
	users := NewMockUser()
	for _, user := range users {
		v := user
		if err := store.Save(v); err != nil {
			return err
		}
	}
	return nil
}
