# 开发环境搭建

- 安装protoc

```shell
brew install protobuf
```

- 安装protoc-gen-go和protoc-gen-go-grpc

```shell
go get google.golang.org/protobuf/cmd/protoc-gen-go
go get google.golang.org/grpc/cmd/protoc-gen-go-grpc
```

- 安装protoc-gen-grpc-gateway和protoc-gen-openapiv2

```shell
go get github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway
go get github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2
```

# Protoc命令使用

- make gen 

```
protoc --proto_path=proto proto/*.proto  --go_out=:pb --go-grpc_out=:pb --grpc-gateway_out=:pb --openapiv2_out=:swagger
protoc-gen-go: invalid Go import path "pb" for "auth_service.proto"

The import path must contain at least one period ('.') or forward slash ('/') character.
```

[访问：https://developers.google.com/protocol-buffers/docs/reference/go-generated#package](https://developers.google.com/protocol-buffers/docs/reference/go-generated#package)

修复： `option go_package = ".;pb";`

- 命令行解释

- proto_path: proto所在文件
- go_out: 生成go文件
- go-grpc_out: 生成go grpc文件
- grpc-gateway_out: 生成grpc gateway文件
- openapiv2_out: 生成swagger文件

protoc --proto_path=proto proto/*.proto  --go_out=:pb --go-grpc_out=:pb --grpc-gateway_out=:pb --openapiv2_out=:swagger


# Swaager

- [访问： swagerr.io](https://app.swaggerhub.com/welcome)

- Create New -> Import API


# 参考项目

[https://github.com/techschool/pcbook-go](https://github.com/techschool/pcbook-go)
