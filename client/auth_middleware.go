/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package client

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
	"time"
)

type AuthMiddleware struct {
	authClient  *AuthClient
	authMethods map[string]bool
	accessToken string
}

func NewAuthMiddleware(authClient *AuthClient, authMethods map[string]bool, refreshDuration time.Duration, ) (*AuthMiddleware, error) {
	middle := &AuthMiddleware{
		authClient:  authClient,
		authMethods: authMethods,
	}

	err := middle.scheduleRefreshToken(refreshDuration)
	if err != nil {
		return nil, err
	}
	return middle, nil
}

func (m *AuthMiddleware) Unary() grpc.UnaryClientInterceptor {
	return func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		if m.authMethods[method] {
			return invoker(m.attachToken(ctx), method, req, reply, cc, opts...)
		}
		return invoker(ctx, method, req, reply, cc, opts...)
	}
}

func (m *AuthMiddleware) Stream() grpc.StreamClientInterceptor {
	return func(ctx context.Context, desc *grpc.StreamDesc, cc *grpc.ClientConn, method string, streamer grpc.Streamer, opts ...grpc.CallOption) (grpc.ClientStream, error) {
		if m.authMethods[method] {
			return streamer(m.attachToken(ctx), desc, cc, method, opts...)
		}
		return streamer(ctx, desc, cc, method, opts...)
	}
}

func (interceptor *AuthMiddleware) attachToken(ctx context.Context) context.Context {
	return metadata.AppendToOutgoingContext(ctx, "authorization", interceptor.accessToken)
}

func (m *AuthMiddleware) scheduleRefreshToken(refreshDuration time.Duration) error {
	err := m.refreshToken()
	if err != nil {
		return err
	}

	go func() {
		wait := refreshDuration
		for {
			time.Sleep(wait)
			err := m.refreshToken()
			if err != nil {
				wait = time.Second
			} else {
				wait = refreshDuration
			}
		}
	}()

	return nil
}

func (m *AuthMiddleware) refreshToken() error {
	accessToken, err := m.authClient.Login(context.Background())
	if err != nil {
		return err
	}
	m.accessToken = accessToken
	return nil
}